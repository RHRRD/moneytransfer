# MoneyTransfer

Test RESTful API for money transfers between accounts

Install and Run:
```
mvn clean install
java -jar target/transfer-1.0-SNAPSHOT-jar-with-dependencies.jar
```

---

Test subscriberIds: \[1, 2\]

---

Information about subscriber:

GET `localhost:4567/subscribers/${subscriberId}`

---

Get all records in database:

GET `localhost:4567/subscribers/history`

---

Send money from ${subscriberId} to ${subscriberIdTo}

POST `localhost:4567/subscribers/{subscriberIdFrom}/sendMoney`

BODY:
```
{
	"subscriberIdTo": {subscriberIdTo},
	"amount": 10.0
}
```
---

Stop application:

GET localhost:4567/stop