package component

import utils.MockitoKotlin
import entity.Subscriber
import entity.SubscriberMoneyHistory
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mockito
import spark.Request
import spark.Response
import repository.SubscriberMoneyRepository
import java.math.BigDecimal
import java.util.*

class HistoryComponentTest {

    private val repository = Mockito.mock(SubscriberMoneyRepository::class.java)

    private val testable = HistoryComponent(repository)

    private lateinit var request: Request
    private lateinit var response: Response

    @Before
    fun setUp() {
        request = Mockito.mock(Request::class.java)
        response = Mockito.mock(Response::class.java)
    }

    @Test
    fun testProcessEmptyHistory() {
        Mockito.`when`(repository.getSubscriberMoneyHistory(MockitoKotlin.any())).thenReturn(emptyList())

        val result = testable.process(request, response)
        assertNotNull(result)
    }

    @Test
    fun testProcess() {
        val record = SubscriberMoneyHistory(
                1L,
                Subscriber(12L, BigDecimal.ONE),
                Subscriber(13L, BigDecimal.ONE),
                BigDecimal.TEN,
                Date()
        )
        Mockito.`when`(repository.getSubscriberMoneyHistory(MockitoKotlin.any())).thenReturn(listOf(record))

        val result = testable.process(request, response)
        assertFalse(result.isNullOrBlank())
        assertEquals(listOf(record).toString(), result)
    }
}