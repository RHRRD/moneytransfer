package component

import entity.Subscriber
import org.eclipse.jetty.http.HttpStatus
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mockito
import spark.Request
import spark.Response
import repository.SubscriberMoneyRepository
import utils.MockitoKotlin
import java.math.BigDecimal
import java.util.*

class SubscriberComponentTest {

    private val repository = Mockito.mock(SubscriberMoneyRepository::class.java)

    private val testable = SubscriberComponent(repository)

    private lateinit var request: Request
    private lateinit var response: Response

    @Before
    fun setUp() {
        request = Mockito.mock(Request::class.java)
        response = Mockito.mock(Response::class.java)
        Mockito.`when`(request.params("subscriberId")).thenReturn("1")
    }

    @Test
    fun testProcessSubscriberNotFound() {
        Mockito.`when`(repository.findSubscriberBySubscriberId(MockitoKotlin.anyLong())).thenReturn(Optional.empty())

        val result = testable.process(request, response)

        assertNotNull(result)
        Mockito.verify(response).status(HttpStatus.NOT_FOUND_404)
    }

    @Test
    fun testProcess() {
        val subscriber = Subscriber(1L, BigDecimal.ONE)
        Mockito.`when`(repository.findSubscriberBySubscriberId(MockitoKotlin.anyLong())).thenReturn(Optional.of(subscriber))

        val result = testable.process(request, response)

        assertNotNull(result)
        assertEquals(subscriber.toString(), result)
    }

}