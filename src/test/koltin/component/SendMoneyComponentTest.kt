package component

import utils.MockitoKotlin
import entity.Subscriber
import entity.SubscriberMoneyHistory
import exception.TransferMoneyException
import org.eclipse.jetty.http.HttpStatus
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import repository.SubscriberMoneyRepository
import spark.Request
import spark.Response
import java.math.BigDecimal
import java.util.*

class SendMoneyComponentTest {

    private val repository = Mockito.mock(SubscriberMoneyRepository::class.java)

    private val testable = SendMoneyComponent(repository)

    private lateinit var request: Request
    private lateinit var response: Response

    @Before
    fun setUp() {
        request = Mockito.mock(Request::class.java)
        response = Mockito.mock(Response::class.java)
        Mockito.`when`(request.params("subscriberIdFrom")).thenReturn("1")

        val tt = """
            {
	"subscriberIdTo": 2,
	"amount": "10.0"
            }
        """

        Mockito.`when`(request.body()).thenReturn(tt)
    }


    @Test
    fun testProcessWithBadBody() {
        Mockito.`when`(request.body()).thenReturn("")

        val result = testable.process(request, response)
        assertNotNull(result)
        Mockito.verify(response).status(HttpStatus.BAD_REQUEST_400)
    }

    @Test
    fun testProcessButSubscriberNotFound() {
        Mockito.`when`(repository.sendMoney(MockitoKotlin.any())).thenThrow(TransferMoneyException.SubscriberNotFoundException(1))

        val result = testable.process(request, response)
        assertNotNull(result)
        Mockito.verify(response).status(HttpStatus.NOT_FOUND_404)
    }

    @Test
    fun testProcessButSubscriberNotHaveEnoughMoney() {
        Mockito.`when`(repository.sendMoney(MockitoKotlin.any())).thenThrow(TransferMoneyException.SubscriberNotHaveEnoughMoney(1))

        val result = testable.process(request, response)
        assertNotNull(result)
        Mockito.verify(response).status(HttpStatus.BAD_REQUEST_400)
    }

    @Test
    fun testProcessAmountLessZero() {
        Mockito.`when`(repository.sendMoney(MockitoKotlin.any())).thenThrow(TransferMoneyException.AmountLessZero(BigDecimal.valueOf(-10)))

        val result = testable.process(request, response)
        assertNotNull(result)
        Mockito.verify(response).status(HttpStatus.BAD_REQUEST_400)
    }

    @Test
    fun testProcess() {
        val record = SubscriberMoneyHistory(
                id = 1L,
                subscriberIdFrom = Subscriber(1L, BigDecimal.TEN),
                subscriberIdTo = Subscriber(2L, BigDecimal.TEN),
                amount = BigDecimal.TEN,
                operationDate = Date()
        )
        Mockito.`when`(repository.sendMoney(MockitoKotlin.any())).thenReturn(record)

        val result = testable.process(request, response)
        assertNotNull(result)
        assertEquals(record.toString(), result)
    }

}