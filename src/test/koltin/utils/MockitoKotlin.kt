package utils

import org.mockito.Mockito

object MockitoKotlin {

    fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    fun anyLong(): Long {
        Mockito.anyLong()
        return 0
    }

    private fun <T> uninitialized(): T = null as T
}