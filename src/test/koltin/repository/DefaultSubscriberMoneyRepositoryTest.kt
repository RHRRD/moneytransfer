package repository

import api.SubscriberMoneyHistorySearchCriteria
import api.TransferMoney
import entity.Subscriber
import exception.TransferMoneyException
import exception.TransferMoneyException.SubscriberNotFoundException
import exception.TransferMoneyException.SubscriberNotHaveEnoughMoney
import org.junit.*
import java.math.BigDecimal
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import kotlin.test.*


class DefaultSubscriberMoneyRepositoryTest {

    private lateinit var emFactory: EntityManagerFactory
    private lateinit var em: EntityManager

    lateinit var repository: DefaultSubscriberMoneyRepository

    @Before
    fun setUp() {
        emFactory = Persistence.createEntityManagerFactory("money.db")
        em = emFactory.createEntityManager()

        repository = DefaultSubscriberMoneyRepository().also {
            it.entityManager = em
        }
    }

    @After
    fun tearDown() {
        em.close()
        emFactory.close()
    }

    @Test
    fun testAddSubscriber() {
        addSubscriber(BigDecimal.TEN)
    }

    @Test
    fun testAddSubscriberAndFindSubscriberBySubscriberId() {
        val subscriberId1 = addSubscriber(BigDecimal.TEN).subscriberId!!
        val subscriberId2 = addSubscriber(BigDecimal.valueOf(124L)).subscriberId!!

        val subscriber1 = repository.findSubscriberBySubscriberId(subscriberId1).orElse(null)
        val subscriber2 = repository.findSubscriberBySubscriberId(subscriberId2).orElse(null)
        assertNotNull(subscriber1)
        assertEquals(10, subscriber1.amount?.toInt())
        assertNotNull(subscriber2)
        assertEquals(124, subscriber2.amount?.toInt())
        assertNull(repository.findSubscriberBySubscriberId(125L).orElse(null))
    }

    @Test
    fun testSendMoney() {
        val subscriberId1 = addSubscriber(BigDecimal.TEN).subscriberId!!
        val subscriberId2 = addSubscriber().subscriberId!!
        sendMoney(subscriberId1, subscriberId2, BigDecimal.TEN)
    }

    @Test(expected = SubscriberNotHaveEnoughMoney::class)
    fun testSendMoneyWithNotEnoughMoneyException() {
        val subscriberId1 = addSubscriber().subscriberId!!
        val subscriberId2 = addSubscriber().subscriberId!!
        sendMoney(subscriberId1, subscriberId2, BigDecimal.TEN)
    }

    @Test(expected = TransferMoneyException.AmountLessZero::class)
    fun testSendMoneyLessZero() {
        val subscriberId1 = addSubscriber().subscriberId!!
        val subscriberId2 = addSubscriber().subscriberId!!
        sendMoney(subscriberId1, subscriberId2, BigDecimal.valueOf(-10))
    }

    @Test(expected = SubscriberNotFoundException::class)
    fun testSendMoneyWithSubscriberNotFoundException() {
        sendMoney(12L, 13L, BigDecimal.TEN)
    }

    @Test
    fun testFindSubscriberMoneyHistory() {
        val subscriberId1 = addSubscriber(BigDecimal.TEN).subscriberId!!
        val subscriberId2 = addSubscriber().subscriberId!!
        sendMoney(subscriberId1, subscriberId2, BigDecimal.TEN)
        sendMoney(subscriberId2, subscriberId1, BigDecimal.valueOf(5))

        val record1From = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria(subscriberFrom = subscriberId1))
        val record1To = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria(subscriberTo = subscriberId2))
        val record2From = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria(subscriberFrom = subscriberId2))
        val record2To = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria(subscriberTo = subscriberId1))
        val recordWithFullCriteria = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria(subscriberFrom = subscriberId1, subscriberTo = subscriberId2))
        assertNotNull(record1From)
        assertNotNull(record1To)
        assertNotNull(record2From)
        assertNotNull(record2To)
        assertNotNull(recordWithFullCriteria)

        val allRecords = repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria())
        println(allRecords)
        assertFalse(allRecords.isEmpty())
        assertEquals(2, allRecords.size)
    }

    private fun <R> inTx(function: () -> R): R {
        em.transaction.begin()
        return function().also {
            em.transaction.commit()
            em.clear()
        }
    }

    private fun addSubscriber(amount: BigDecimal = BigDecimal.ONE): Subscriber {
        return inTx {
            repository.addSubscriber(Subscriber(amount = amount))
        }
    }

    private fun sendMoney(subscriberFrom: Long, subscriberTo: Long, amount: BigDecimal = BigDecimal.ONE) {
        inTx {
            repository.sendMoney(TransferMoney(subscriberIdFrom = subscriberFrom, subscriberIdTo = subscriberTo, amount = amount))
        }
    }

}