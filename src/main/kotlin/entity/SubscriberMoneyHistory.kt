package entity

import lombok.Data
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Data
@Entity(name = "SUBSCRIBER_MONEY_HISTORY")
data class SubscriberMoneyHistory(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SUBS_MONEY_HIST_ID")
    val id: Long? = null,

    @OneToOne
    @JoinColumn(name = "SUBSCRIBER_ID_FROM", referencedColumnName = "SUBSCRIBER_ID")
    val subscriberIdFrom: Subscriber? = null,

    @OneToOne
    @JoinColumn(name = "SUBSCRIBER_ID_TO", referencedColumnName = "SUBSCRIBER_ID")
    val subscriberIdTo: Subscriber? = null,

    @Column(name = "AMOUNT")
    val amount: BigDecimal? = null,

    @Column(name = "OPERATION_DATE")
    val operationDate: Date? = null

)