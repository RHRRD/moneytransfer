package entity

import lombok.Data
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.*

@Data
@Entity(name = "SUBSCRIBER_MONEY")
data class Subscriber(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SUBSCRIBER_ID")
    val subscriberId: Long? = null,

    @Column(name = "AMOUNT")
    var amount: BigDecimal? = null

) : Serializable