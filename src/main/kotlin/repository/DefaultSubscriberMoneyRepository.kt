package repository

import api.SubscriberMoneyHistorySearchCriteria
import api.TransferMoney
import entity.Subscriber
import entity.SubscriberMoneyHistory
import exception.TransferMoneyException
import exception.TransferMoneyException.*
import java.math.BigDecimal
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

class DefaultSubscriberMoneyRepository : SubscriberMoneyRepository {

    @PersistenceContext(unitName = "money.db")
    lateinit var entityManager: EntityManager

    @Transactional
    override fun addSubscriber(subscriber: Subscriber): Subscriber {
        entityManager.persist(subscriber)
        return subscriber
    }

    override fun findSubscriberBySubscriberId(subscriberId: Long): Optional<Subscriber> {
        val query = entityManager.createQuery("SELECT subs FROM SUBSCRIBER_MONEY subs WHERE subs.subscriberId = $subscriberId", Subscriber::class.java)
        val subscriber = query.resultList

        return Optional.ofNullable(subscriber.firstOrNull())
    }

    @Transactional
    override fun sendMoney(transferMoney: TransferMoney): SubscriberMoneyHistory {
        if (transferMoney.amount < BigDecimal.ZERO) throw AmountLessZero(transferMoney.amount)

        val subscriberFrom = findSubscriberBySubscriberId(transferMoney.subscriberIdFrom)
                .orElseThrow { SubscriberNotFoundException(transferMoney.subscriberIdFrom) }
        if (subscriberFrom.amount!! < transferMoney.amount) throw SubscriberNotHaveEnoughMoney(subscriberFrom.subscriberId!!)
        val subscriberTo = findSubscriberBySubscriberId(transferMoney.subscriberIdTo)
                .orElseThrow { SubscriberNotFoundException(transferMoney.subscriberIdTo) }
        takeMoney(subscriberFrom, transferMoney.amount)
        giveMoney(subscriberTo, transferMoney.amount)
        val subscriberMoneyHistory = SubscriberMoneyHistory(
                subscriberIdFrom = subscriberFrom,
                subscriberIdTo = subscriberTo,
                amount = transferMoney.amount,
                operationDate = Date()
        )
        entityManager.persist(subscriberMoneyHistory)
        return subscriberMoneyHistory
    }

    override fun getSubscriberMoneyHistory(searchCriteria: SubscriberMoneyHistorySearchCriteria): List<SubscriberMoneyHistory> {
        var queryString = "SELECT smh FROM SUBSCRIBER_MONEY_HISTORY smh where 1=1 "
        if (searchCriteria.subscriberFrom != null) queryString += " and smh.subscriberIdFrom = ${searchCriteria.subscriberFrom}"
        if (searchCriteria.subscriberTo != null) queryString += " and smh.subscriberIdTo = ${searchCriteria.subscriberTo}"
        val query = entityManager.createQuery(queryString, SubscriberMoneyHistory::class.java)

        return query.resultList
    }

    private fun takeMoney(subscriber: Subscriber, amount: BigDecimal) {
        entityManager.merge(subscriber.also { it.amount = it.amount!!.minus(amount) })
    }

    private fun giveMoney(subscriber: Subscriber, amount: BigDecimal) {
        entityManager.merge(subscriber.also { it.amount = it.amount!!.plus(amount) })
    }

}