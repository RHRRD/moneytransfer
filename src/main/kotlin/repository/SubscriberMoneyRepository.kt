package repository

import api.SubscriberMoneyHistorySearchCriteria
import api.TransferMoney
import entity.Subscriber
import entity.SubscriberMoneyHistory
import java.util.*

interface SubscriberMoneyRepository {

    fun addSubscriber(subscriber: Subscriber): Subscriber

    fun findSubscriberBySubscriberId(subscriberId: Long): Optional<Subscriber>

    fun sendMoney(transferMoney: TransferMoney): SubscriberMoneyHistory

    fun getSubscriberMoneyHistory(searchCriteria: SubscriberMoneyHistorySearchCriteria): List<SubscriberMoneyHistory>

}