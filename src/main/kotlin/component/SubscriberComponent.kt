package component

import exception.TransferMoneyException.SubscriberNotFoundException
import mu.KLogging
import org.eclipse.jetty.http.HttpStatus
import repository.SubscriberMoneyRepository
import spark.Request
import spark.Response

class SubscriberComponent(private val repository: SubscriberMoneyRepository) {

    fun process(request: Request, response: Response): String? {
        logger.info { "request $request response $response" }
        val subscriberId = request.params("subscriberId").toLong()

        val subscriber = repository.findSubscriberBySubscriberId(subscriberId)

        if (!subscriber.isPresent) {
            response.status(HttpStatus.NOT_FOUND_404)
            return SubscriberNotFoundException(subscriberId).message
        }
        return subscriber.get().toString()
    }

    companion object : KLogging()

}