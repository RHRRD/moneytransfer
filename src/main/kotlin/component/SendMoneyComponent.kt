package component

import api.TransferMoney
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import exception.TransferMoneyException
import exception.TransferMoneyException.*
import mu.KLogging
import org.eclipse.jetty.http.HttpStatus
import repository.SubscriberMoneyRepository
import spark.Request
import spark.Response
import java.lang.Exception

class SendMoneyComponent(private val repository: SubscriberMoneyRepository) {

    fun process(request: Request, response: Response): String? {
        logger.info { "request $request response $response" }
        val transferMoneyFormBody = try {
            mapper.readValue(request.body(), TransferMoney::class.java)
        } catch (ex: Exception) {
            logger.error(ex) { ex.message }
            response.status(HttpStatus.BAD_REQUEST_400)
            return ex.message
        }
        return try {
            repository.sendMoney(TransferMoney(
                    subscriberIdFrom = request.params("subscriberIdFrom").toLong(),
                    subscriberIdTo = transferMoneyFormBody.subscriberIdTo,
                    amount = transferMoneyFormBody.amount
            )).toString()
        } catch (ex: TransferMoneyException) {
            logger.error(ex) { ex.message }
            when (ex) {
                is SubscriberNotFoundException -> response.status(HttpStatus.NOT_FOUND_404)
                is SubscriberNotHaveEnoughMoney, is AmountLessZero -> response.status(HttpStatus.BAD_REQUEST_400)
            }
            ex.message
        }
    }

    companion object : KLogging() {
        private val mapper = jacksonObjectMapper()
    }

}