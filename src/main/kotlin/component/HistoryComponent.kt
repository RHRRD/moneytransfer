package component

import api.SubscriberMoneyHistorySearchCriteria
import mu.KLogging
import repository.SubscriberMoneyRepository
import spark.Request
import spark.Response

class HistoryComponent(private val repository: SubscriberMoneyRepository) {

    fun process(request: Request, response: Response): String? {
        logger.info { "request $request response $response" }

        return repository.getSubscriberMoneyHistory(SubscriberMoneyHistorySearchCriteria()).toString()
    }


    companion object : KLogging()

}