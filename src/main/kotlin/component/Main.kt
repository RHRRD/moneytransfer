package component

import entity.Subscriber
import repository.DefaultSubscriberMoneyRepository
import spark.Spark.*
import java.math.BigDecimal
import javax.persistence.EntityManager
import javax.persistence.Persistence

fun main(args: Array<String>) {

    val entityManagerFactory = Persistence.createEntityManagerFactory("money.db")
    val entityManager = entityManagerFactory.createEntityManager()

    val repository = DefaultSubscriberMoneyRepository().also {
        it.entityManager = entityManager
    }

    setUpDatabase(entityManager, repository)

    val sendMoneyComponent = SendMoneyComponent(repository)
    val subscriberComponent = SubscriberComponent(repository)
    val historyComponent = HistoryComponent(repository)

    path("/subscribers") {

        get("/history") { request, response ->
            historyComponent.process(request, response)
        }

        get("/:subscriberId") { request, response ->
            subscriberComponent.process(request, response)
        }


        post("/:subscriberIdFrom/sendMoney") { request, response ->
            inTx(entityManager) {
                sendMoneyComponent.process(request, response)
            }
        }

    }

    get("/stop") { request, response ->
        stop()
        entityManager.clear()
        entityManagerFactory.close()
        System.exit(0)
    }
}

private fun setUpDatabase(entityManager: EntityManager, repository: DefaultSubscriberMoneyRepository) {
    inTx(entityManager) {
        repository.addSubscriber(Subscriber(amount = BigDecimal.valueOf(100)))
        repository.addSubscriber(Subscriber(amount = BigDecimal.valueOf(50)))
    }
}

private fun <R> inTx(em: EntityManager, function: () -> R): R {
    em.transaction.begin()
    return function().also {
        em.transaction.commit()
        em.clear()
    }
}

