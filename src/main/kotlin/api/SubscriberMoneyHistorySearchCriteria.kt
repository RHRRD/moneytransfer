package api

data class SubscriberMoneyHistorySearchCriteria(
    val subscriberFrom: Long? = null,
    val subscriberTo: Long? = null
)