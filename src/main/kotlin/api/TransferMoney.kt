package api

import java.math.BigDecimal

data class TransferMoney(
        val subscriberIdFrom: Long,
        val subscriberIdTo: Long,
        val amount: BigDecimal
)