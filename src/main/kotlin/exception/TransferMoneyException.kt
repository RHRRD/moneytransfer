package exception

import java.lang.RuntimeException
import java.math.BigDecimal

sealed class TransferMoneyException(message: String) : RuntimeException(message) {
    class SubscriberNotFoundException(subscriberId: Long) : TransferMoneyException("Subscriber with id: $subscriberId not found.")
    class SubscriberNotHaveEnoughMoney(subscriberId: Long) : TransferMoneyException("Subscriber with id: $subscriberId does not have enough money.")
    class AmountLessZero(amount: BigDecimal) : TransferMoneyException("Amount $amount less 0.")
}